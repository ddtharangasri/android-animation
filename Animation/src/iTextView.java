import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;


public class iTextView extends TextView {

    public iTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    public iTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public iTextView(Context context) {
        super(context);
        init();
    }

    public void init() {
        Typeface tf = Typeface.createFromAsset(getContext().getAssets(), "font/museo_slab.ttf");
        setTypeface(tf ,1);

    }

}
