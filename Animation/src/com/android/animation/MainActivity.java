package com.android.animation;


import com.android.animation.R;

import android.support.v7.app.ActionBarActivity;
import android.annotation.SuppressLint;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.widget.RelativeLayout;
import android.widget.ViewFlipper;


public class MainActivity extends ActionBarActivity {

		int transitionTime = 1000;

		private RelativeLayout Rlayout;

		
		

		@SuppressLint({ "NewApi", "CutPasteId" }) @Override
	    protected void onCreate(Bundle savedInstanceState) {
	        super.onCreate(savedInstanceState);
	        setContentView(R.layout.activity_main);
	        
	        
	        
	        ViewFlipper flipper = (ViewFlipper) findViewById(R.id.flipper1);
			
			Rlayout = (RelativeLayout)findViewById(R.id.relativeLayoutMain);


			Drawable color_01 = this.getResources().getDrawable(R.anim.color_selector_01);
			Drawable color_02 = this.getResources().getDrawable(R.anim.color_selector_02);
			Drawable color_03 = this.getResources().getDrawable(R.anim.color_selector_03);
			Drawable color_04 = this.getResources().getDrawable(R.anim.color_selector_04);
			Drawable color_05 = this.getResources().getDrawable(R.anim.color_selector_05);
			
			CyclicTransitionDrawable ctd = new CyclicTransitionDrawable(new Drawable[] { 
					color_01, 
					color_02,
					color_03,
					color_04,
					color_05
					},flipper);

			Rlayout.setBackgroundDrawable(ctd);

			ctd.startTransition(1000, 3000);
			
	      ////Animation //////////////////////////////////////////////////
	        
	       // ViewFlipper flipper = (ViewFlipper) findViewById(R.id.flipper1);
	        //flipper.setFlipInterval(4500);
			//flipper.startFlipping();
			
			////////////////////////////////////////////////////////////


			
	    }
    

    

}
